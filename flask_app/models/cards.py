from flask_app.config.mysqlconnection import connectToMySQL
from flask_app.models import development

class Cards:
    def __init__(self,data):
        self.id = data['id']
        self.name= data['name']
        self.observation = data['observation']
        self.difficulty = data['difficulty']
        self.developer_id = data ['developer_id']
        self.created_at = data['created_at']
        self.updated_at = data['updated_at']
       

    @classmethod
    def save(cls,data):
        query = "INSERT INTO cards (name,observation,difficulty,developer_id,created_at,updated_at) VALUES (%(name)s,%(observation)s,%(difficulty)s,%(developer_id)s,NOW(),NOW())"
        return connectToMySQL('cards_schema').query_db(query,data)

    @classmethod
    def get_all(cls):
        query = "SELECT * FROM cards;"
        cards_from_db =  connectToMySQL('cards_schema').query_db(query)
        cards =[]
        for b in cards_from_db:
            cards.append(cls(b))
        return cards

    @classmethod
    def get_one(cls,data):
        query = "SELECT * FROM cards WHERE cards.id = %(id)s;"
        cards_from_db = connectToMySQL('cards_schema').query_db(query,data)

        return cls(cards_from_db[0])

    @classmethod
    def update(cls,data):
        query = "UPDATE cards SET name=%(name)s, observation=%(observation)s, difficulty=%(difficulty)s,developer_id=%(developer_id)s,updated_at = NOW() WHERE id = %(id)s;"
        return connectToMySQL('cards_schema').query_db(query,data)

    @classmethod
    def destroy(cls,data):
        query = "DELETE FROM cards WHERE id = %(id)s;"
        return connectToMySQL('cards_schema').query_db(query,data)
    
    
