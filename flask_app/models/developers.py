from flask_app.config.mysqlconnection import connectToMySQL
from flask_app.models import cards

class Developers:
    def __init__(self , db_data ):
        self.id = db_data['id']
        self.name = db_data['name']
        self.email = db_data['email']
        self.created_at = db_data['created_at']
        self.updated_at = db_data['updated_at']
        # We create a list so that later we can add in all the burgers that are associated with a restaurant.
        self.cards = []
    @classmethod
    def save( cls , data ):
        query = "INSERT INTO developers ( name ,email , created_at , updated_at ) VALUES (%(name)s,%(email)s,NOW(),NOW());"
        return connectToMySQL('tasks_schema').query_db( query, data)

    @classmethod
    def get_developers_with_cards( cls , data ):
        query = "SELECT * FROM developers LEFT JOIN cards ON cards.developer_id = developers.id WHERE developers.id = %(id)s;"
        results = connectToMySQL('tasks_schema').query_db( query , data )
        # results will be a list of topping objects with the burger attached to each row. 
        developers = cls( results[0] )
        for row_from_db in results:
            # Now we parse the burger data to make instances of burgers and add them into our list.
            cards_data = {
                "id" : row_from_db["cards.id"],
                "name" : row_from_db["cards.name"],
                "difficulty" : row_from_db["cards.difficulty"],
                "created_at" : row_from_db["cards.created_at"],
                "updated_at" : row_from_db["cards.updated_at"]
            }
            developers.cards.append( cards.cards( cards_data ) )
        return developer
    
    @classmethod
    def get_all(cls):
        query = "SELECT * FROM developers;"
        results = connectToMySQL('tasks_schema').query_db(query)
        return results
    
    @classmethod
    def get_one(cls,data):
        query = "SELECT name FROM developers with id = %(id)s;"
        results = connectToMySQL('tasks_schema').query_db(query,data)
        return cls(results[0])