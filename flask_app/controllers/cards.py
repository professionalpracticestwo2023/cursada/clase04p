from flask_app import app
from flask import render_template, redirect, request, session, session, flash
from flask_app.models.cards import Cards
from flask_app.models.developers import Developers

@app.route('/')
def index():
    return render_template("index.html",developers=Developers.get_all())

@app.route('/create',methods=['POST'])
def create():
    data = {
        "name":request.form['name'],
        "observation": request.form['observation'],
        "difficulty": request.form['difficulty'],
        "developer_id": request.form['developer_id']
    }
    Cards.save(data)
    return redirect('/cards')

@app.route('/cards')
def Card():
    return render_template("results.html",all_cards=Cards.get_all())


@app.route('/show/<int:cards_id>')
def detail_page(cards_id):
    data = {
        'id': cards_id
    }
    return render_template("details_page.html",cards=Cards.get_one(data))

@app.route('/edit_page/<int:cards_id>')
def edit_page(cards_id):
    data = {
        'id': cards_id
    }
    return render_template("edit_page.html", cards = Cards.get_one(data),developers=Developers.get_all())

@app.route('/update/<int:cards_id>', methods=['POST'])
def update(cards_id):
    data = {
        'id': cards_id,
        "name":request.form['name'],
        "observation": request.form['observation'],
        "difficulty": request.form['difficulty'],
        "developer_id": request.form['developer_id']
    }
    Cards.update(data)
    return redirect(f"/show/{cards_id}")

@app.route('/delete/<int:cards_id>')
def delete(cards_id):
    data = {
        'id': cards_id,
    }
    Cards.destroy(data)
    return redirect('/cards')