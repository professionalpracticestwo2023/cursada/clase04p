### Crud realizado con Python + Flask + Mysql
### El mismo utiliza el patro MVC

### Para el correcto funcionamiento deberán crear una estructura como la siguiente:

DROP SCHEMA IF EXISTS `tasks` ;

-- -----------------------------------------------------
-- Schema tasks
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tasks_schema` DEFAULT CHARACTER SET utf8 ;
USE `tasks_schema` ;

-- -----------------------------------------------------
-- Table `tasks_schema`.`developers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tasks_schema`.`developers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `email` VARCHAR(255) NULL,
  `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tasks_schema`.`cards`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tasks_schema`.`cards` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `observation` VARCHAR(255) NULL,
  `difficulty` INT NULL,
  `developer_id` INT NOT NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tasks_schema`.`development`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tasks_schema`.`development` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `development_name` VARCHAR(255) NULL,
  `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;